<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use VitaliiLuka\MentorshipBlog\Common\Entity\Traits\IdentifiableEntityTrait;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\Post\Entity\Category;
use VitaliiLuka\MentorshipBlog\User\Repository\SubscriberRepository;

/**
 * @ORM\Entity(repositoryClass=SubscriberRepository::class)
 */
class Subscriber
{
    use IdentifiableEntityTrait;

    /**
     * @ORM\OneToOne(targetEntity=User::class)
     */
    private User $user;

    /**
     * @var Category[]|Collection
     *
     * @ORM\ManyToMany(targetEntity=Category::class)
     */
    private Collection $categories;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $lastRunDateTime;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->categories = new ArrayCollection();
        $this->lastRunDateTime = new \DateTime();
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Category[]|Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * @param Category[]|Collection $categories
     */
    public function setCategories(Collection $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getLastRunDateTime(): \DateTime
    {
        return $this->lastRunDateTime;
    }

    public function setLastRunDateTime(\DateTime $lastRunDateTime): self
    {
        $this->lastRunDateTime = $lastRunDateTime;

        return $this;
    }

    public function addCategory(Category $category): self
    {
        $this->categories->add($category);

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }
}
