<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;

class UserFixtures extends Fixture
{
    public const POST_AUTHOR = 'post-author';

    /**
     * UserFixtures constructor.
     * @param UserPasswordHasherInterface $passwordHasher
     * @ORM\Column(type="string")
     */
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User(
            'test@test.ua',
            '12345678',
            'First Name',
            'Last Name',
            false
        );
        $user->setPassword($this->passwordHasher->hashPassword($user, '12345678'));

        $this->addReference(self::POST_AUTHOR, $user);

        $manager->persist($user);
        $manager->flush();
    }
}
