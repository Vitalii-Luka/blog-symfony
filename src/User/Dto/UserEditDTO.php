<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Dto;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class UserEditDTO
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=64)
     */
    private ?string $firstName = null;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=64)
     */
    private ?string $lastName = null;

    /**
     * @Assert\Image(
     *     maxHeight=400,
     *     maxWidth=400,
     *     mimeTypes={"image/jpeg", "image/png" },
     *     mimeTypesMessage="Please upload a jpeg or png image",
     *     maxSize="4M",
     * )
     */
    private ?UploadedFile $avatar = null;

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAvatar(): ?UploadedFile
    {
        return $this->avatar;
    }

    public function setAvatar(?UploadedFile $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }
}
