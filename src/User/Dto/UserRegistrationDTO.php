<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints as SymfonyConstraints;
use VitaliiLuka\MentorshipBlog\Common\Validator\Constraints\UniqueDTOProperty;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;

class UserRegistrationDTO
{
    /**
     * @SymfonyConstraints\NotBlank()
     * @SymfonyConstraints\Length(max=254)
     * @SymfonyConstraints\Email()
     * @UniqueDTOProperty(entityClass=User::class, entityProperty="email")
     */
    private ?string $email = null;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=8, max=64)
     */
    private ?string $plainPassword;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=64)
     */
    private ?string $firstName;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=64)
     */
    private ?string $lastName;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @return ?string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param ?string $firstName
     * @return UserRegistrationDTO
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return ?string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param ?string $lastName
     * @return UserRegistrationDTO
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }
}
