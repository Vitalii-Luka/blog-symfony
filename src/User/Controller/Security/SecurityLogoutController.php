<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Controller\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecurityLogoutController extends AbstractController
{
    /**
     * @Route("/logout", name="app_logout")
     */
    public function __invoke(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
