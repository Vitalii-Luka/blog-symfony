<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function __invoke(EntityManagerInterface $manager)
    {
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedException();
        }

        $user = $manager->getRepository(User::class)->find($user->getId());

        if (!$user) {
            throw new AccessDeniedException();
        }

        return $this->render('main/user.html.twig', [
            'user' => $user,
        ]);
    }
}
