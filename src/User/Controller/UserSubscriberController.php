<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\User\Entity\Subscriber;
use VitaliiLuka\MentorshipBlog\User\Form\SubscriberType;

class UserSubscriberController extends AbstractController
{
    private $repository;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->repository = $manager->getRepository(Subscriber::class);
    }

    /**
     * @Route("/user/subscriber", name="user_subscriber")
     */
    public function __invoke(Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedException();
        }

        $user = $manager->getRepository(User::class)->find($user->getId());
        if (!$user) {
            throw new AccessDeniedException();
        }

        $subscriber = $this->repository->findOneBy(['user' => $user->getId()]);

        if (!$subscriber) {
            $subscriber = new Subscriber($user);
        }

        $form = $this->createForm(SubscriberType::class, $subscriber);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($subscriber);
            $manager->flush();
            $this->addFlash('success', 'subscription successfully created!');

            return $this->redirectToRoute('user');
        }

        $form = $this->createForm(SubscriberType::class);

        return $this->render('main/user/subscriber-form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
