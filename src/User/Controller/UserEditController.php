<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\Core\Service\FileUploader;
use VitaliiLuka\MentorshipBlog\User\Dto\UserEditDTO;
use VitaliiLuka\MentorshipBlog\User\Form\UserEditType;

class UserEditController extends AbstractController
{
    private FileUploader $fileUploader;

    /**
     * @Route("/user/edit", name="user_edit")
     */
    public function __invoke(Request $request, EntityManagerInterface $manager, FileUploader $fileUploader): Response
    {
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedException();
        }

        $user = $manager->getRepository(User::class)->find($user->getId());

        if (!$user) {
            throw new AccessDeniedException();
        }

        $dto = new UserEditDTO();
        $dto->setFirstName($user->getFirstName());
        $dto->setLastName($user->getLastName());

        $form = $this->createForm(UserEditType::class, $dto);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $avatar = $dto->getAvatar();

            if ($avatar) {
                $avatarFileName = $fileUploader->upload($avatar);
                $user->setAvatar($avatarFileName);
            }

            $user->setFirstName($dto->getFirstName())->setLastName($dto->getLastName());

            $manager->flush();

            $this->addFlash('success', 'Profile successfully updated!');

            return $this->redirectToRoute('user');
        }

        return $this->render('main/user/editform.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
