<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\User\Dto\UserRegistrationDTO;
use VitaliiLuka\MentorshipBlog\User\Form\UserRegistrationType;
use VitaliiLuka\MentorshipBlog\User\Service\UserManager;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/registration", name="user_registration")
     * @return RedirectResponse|Response
     */
    public function __invoke(Request $request, UserManager $userManager)
    {
        $dto = new UserRegistrationDTO();
        $form = $this->createForm(UserRegistrationType::class, $dto);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $dto = $form->getData();
            $userManager->create($dto);

            return $this->redirectToRoute('home');
        }

        return $this->render('main/user/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
