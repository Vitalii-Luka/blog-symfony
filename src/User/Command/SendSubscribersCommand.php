<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;
use VitaliiLuka\MentorshipBlog\User\Repository\SubscriberRepository;
use VitaliiLuka\MentorshipBlog\User\Service\NotificationSender;

class SendSubscribersCommand extends Command
{
    protected static $defaultName = 'send:subscribers:notification';

    private NotificationSender $notificationSender;

    private SubscriberRepository $subscriberRepository;

    private EntityManagerInterface $manager;

    public function __construct(
        EntityManagerInterface $manager,
        NotificationSender $notificationSender,
        SubscriberRepository $subscriberRepository
    ) {
        parent::__construct();
        $this->notificationSender = $notificationSender;
        $this->subscriberRepository = $subscriberRepository;
        $this->manager = $manager;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->subscriberRepository->findAll() as $subscriber) {
            $posts = $this->manager
                ->getRepository(Post::class)
                ->createQueryBuilder('p')
                ->where('p.category IN (:categories)')
                ->andWhere('p.createdAt >= :from')
                ->setParameters([
                    'from' => $subscriber->getLastRunDateTime(),
                    'categories' => $subscriber->getCategories(),
                ])
                ->getQuery()
                ->getResult()
            ;

            if (!$posts) {
                continue;
            }

            $this->notificationSender->sendSubscriberNotification($subscriber, $posts);
            $subscriber->setLastRunDateTime(new \DateTime());
        }

        $this->manager->flush();

        return 0;
    }
}
