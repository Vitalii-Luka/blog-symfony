<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use VitaliiLuka\MentorshipBlog\User\Entity\Subscriber;

class NotificationSender
{
    private MailerInterface $mailer;

    private string $emailFrom;

    public function __construct(MailerInterface $mailer, string $emailFrom)
    {
        $this->mailer = $mailer;
        $this->emailFrom = $emailFrom;
    }

    public function sendSubscriberNotification(Subscriber $subscriber, array $posts): void
    {
        $email = (new TemplatedEmail())
            ->from($this->emailFrom)
            ->to($subscriber->getUser()->getEmail())
            ->subject('Subject')
            ->htmlTemplate('user/notification.html.twig')
            ->context([
                'posts' => $posts,
            ])
        ;
        $this->mailer->send($email);
    }
}
