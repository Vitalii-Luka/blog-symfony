<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\User\Dto\UserRegistrationDTO;

class UserManager
{
    private PasswordHasherFactoryInterface $passwordHasherFactory;

    private EntityManagerInterface $em;

    public function __construct(PasswordHasherFactoryInterface $passwordHasherFactory, EntityManagerInterface $em)
    {
        $this->passwordHasherFactory = $passwordHasherFactory;
        $this->em = $em;
    }

    public function create(UserRegistrationDTO $dto): User
    {
        $user = new User(
            $dto->getEmail(),
            $this->getHashedPassword((string) $dto->getPlainPassword()),
            $dto->getFirstName(),
            $dto->getLastName(),
        );

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param mixed $id
     * @return object|User|null
     */
    public function find($id)
    {
        return $this->em->getRepository(User::class)->find($id);
    }

    private function getHashedPassword(string $plainPassword): string
    {
        return $this->passwordHasherFactory->getPasswordHasher(User::class)
            ->hash($plainPassword)
            ;
    }
}
