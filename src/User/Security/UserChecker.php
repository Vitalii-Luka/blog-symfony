<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\User\Security;

use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use VitaliiLuka\MentorshipBlog\Core\Entity\User as AppUser;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {
    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if ($user->getBlocked()) {
            throw new AccountExpiredException('Your user account is blocked!');
        }
    }
}
