<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Core\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use VitaliiLuka\MentorshipBlog\Core\DTO\ContactUsMessagesDTO;

class ContactUsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Email', EmailType::class, [
                'label' => 'Email',
                'attr' => [
                    'placeholder' => 'Enter Email',
                ],
            ])
            ->add('Name', TextType::class, [
                'label' => 'Name',
                'attr' => [
                    'placeholder' => 'Enter Name',
                ],
            ])
            ->add('Message', TextareaType::class, [
                'label' => 'Message',
                'attr' => [
                    'placeholder' => 'Write The Message',
                ],
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContactUsMessagesDTO::class,
        ]);
    }
}
