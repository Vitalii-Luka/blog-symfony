<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Core\DTO;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints as SymfonyConstraints;

class ContactUsMessagesDTO
{
    /**
     * @SymfonyConstraints\NotBlank()
     * @SymfonyConstraints\Length(max=254)
     * @SymfonyConstraints\Email()
     */
    private ?string $email = null;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=64)
     */
    private ?string $name = null;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=8, max=10000)
     */
    private ?string $message;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }
}
