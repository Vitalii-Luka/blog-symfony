<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Core;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use VitaliiLuka\MentorshipBlog\Core\Entity\ContactUsMessages;

class SpamChecker
{
    public const NOT_SPAM_SCORE = 0;
    public const MAYBE_SPAM_SCORE = 1;
    public const SPAM_SCORE = 2;

    private HttpClientInterface $client;

    private string $hostname;

    private string $endpoint;

    public function __construct(HttpClientInterface $client, string $hostname, string $akismetKey)
    {
        $this->client = $client;
        $this->hostname = $hostname;
        $this->endpoint = sprintf('https://%s.rest.akismet.com/1.1/comment-check', $akismetKey);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return int Spam score: 0: not spam, 1: maybe spam, 2: blatant spam
     */
    public function getSpamScore(ContactUsMessages $contact, array $context): int
    {
        $response = $this->client->request('POST', $this->endpoint, [
            'body' => array_merge($context, [
                'blog' => sprintf('%s://%s', 'https', $this->hostname),
                'contact_type' => 'contact',
                'contact_author' => $contact->getName(),
                'contact_author_email' => $contact->getEmail(),
                'contact_content' => $contact->getMessage(),
                'blog_lang' => 'en',
                'blog_charset' => 'UTF-8',
                'is_test' => true,
            ]),
        ]);

        $headers = $response->getHeaders();
        if ('discard' === ($headers['x-akismet-pro-tip'][0] ?? '')) {
            return self::SPAM_SCORE;
        }

        $content = $response->getContent();
        if (isset($headers['x-akismet-debug-help'][0])) {
            throw new \RuntimeException(sprintf('Unable to check for spam: %s (%s).', $content, $headers['x-akismet-debug-help'][0]));
        }

        return $content === 'true' ? self::MAYBE_SPAM_SCORE : self::NOT_SPAM_SCORE;
    }
}
