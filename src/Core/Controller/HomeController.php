<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Core\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\Post\Dto\PostSearchData;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;
use VitaliiLuka\MentorshipBlog\Post\Form\PostSearchDataType;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function __invoke(EntityManagerInterface $em, Request $request, PaginatorInterface $paginator): Response
    {
        $searchData = new PostSearchData();

        $form = $this->createForm(
            PostSearchDataType::class,
            $searchData,
            ['action' => $this->generateUrl('home')]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $query = $em->getRepository(Post::class)->searchByPartialData($searchData->getValue());
        } else {
            $query = $em->getRepository(Post::class)->findAllQuery();
        }

        return $this->render('core/home.html.twig', [
            'posts' => $paginator
                ->paginate(
                    $query,
                    $request->query->getInt('page', 1),
                    5
                ),
            'searchForm' => $form->createView(),
        ]);
    }
}
