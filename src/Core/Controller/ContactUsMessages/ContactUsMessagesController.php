<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Core\Controller\ContactUsMessages;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\Core\DTO\ContactUsMessagesDTO;
use VitaliiLuka\MentorshipBlog\Core\Entity\ContactUsMessages;
use VitaliiLuka\MentorshipBlog\Core\Form\ContactUsType;
use VitaliiLuka\MentorshipBlog\Core\SpamChecker;

class ContactUsMessagesController extends AbstractController
{
    /**
     * @Route("/contact", name="core_contact-us-messages_create")
     */
    public function __invoke(Request $request, EntityManagerInterface $manager, SpamChecker $spamChecker): Response
    {
        $dto = new ContactUsMessagesDTO();

        $user = $this->getUser();
        if ($user) {
            $dto->setEmail($user->getEmail());
            $dto->setName($user->getLastName() . ' ' . $user->getFirstName());
        }

        $form = $this->createForm(ContactUsType::class, $dto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dto = $form->getData();

            $contact = new ContactUsMessages(
                $dto->getEmail(),
                $dto->getName(),
                $dto->getMessage(),
            );

            $manager->persist($contact);

            $context = [
                'user_ip' => $request->getClientIp(),
                'user_agent' => $request->headers->get('user-agent'),
                'referrer' => $request->headers->get('referer'),
                'permalink' => $request->getUri(),
            ];

            if ($spamChecker->getSpamScore($contact, $context) === SpamChecker::SPAM_SCORE) {
                $this->addFlash('danger', 'Blatant spam, go away!');

                return $this->redirectToRoute('home');
            }

            if ($spamChecker->getSpamScore($contact, $context) === SpamChecker::MAYBE_SPAM_SCORE) {
                $this->addFlash('warning', 'Warning, maybe spam!');

                return $this->redirectToRoute('home');
            }

            if ($spamChecker->getSpamScore($contact, $context) === SpamChecker::NOT_SPAM_SCORE) {
                $manager->flush();

                $this->addFlash('success', 'Message sent successfully!');

                return $this->redirectToRoute('home');
            }
        }

        return $this->render('core/contact/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
