<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use VitaliiLuka\MentorshipBlog\Common\Entity\Traits\IdentifiableEntityTrait;
use VitaliiLuka\MentorshipBlog\Core\Repository\ContactUsMessagesRepository;

/**
 * @ORM\Entity(repositoryClass=ContactUsMessagesRepository::class)
 */
class ContactUsMessages
{
    use IdentifiableEntityTrait;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private string $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="text", length=10000)
     */
    private string $message;

    public function __construct(string $email, string $name, string $message)
    {
        $this->email = $email;
        $this->name = $name;
        $this->message = $message;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }
}
