<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Core\Menu;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use VitaliiLuka\MentorshipBlog\Core\Entity\Category;

class MenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private FactoryInterface $factory;

    private AuthorizationCheckerInterface $authorizationChecker;

    private EntityManagerInterface $em;

    public function __construct(FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->factory = $factory;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createMainMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            $menu->addChild('My profile');

            $menu['My profile']->addChild('Edit my profile', [
                'route' => 'home',
            ]);

            $menu->addChild('Posts');
            $menu['Posts']->addChild('Show my posts', [
                'route' => 'show_post',
            ]);
            $menu['Posts']->addChild('Add post', [
                'route' => 'create_post',
            ]);
        }

        return $menu;
    }

    public function createCategoryDropdownMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $menu->addChild('Category');
        $menu['Category']->addChild('Category', [
            'route' => 'home',
        ]);

//            $items = $this->getDoctrine()->getRepository(Category::class)->findAll();
//
//            foreach ($items as $item) {
//                $menu['Category']->addChild($item->getName(), [
//                    'route' => 'category',
//                    'routeParameters' => ['id' => $item->getId()]
//                ]);
//            }

        return $menu;
    }
}
