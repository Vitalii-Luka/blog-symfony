<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Core\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use VitaliiLuka\MentorshipBlog\Core\Entity\ContactUsMessages;

/**
 * @method ContactUsMessages|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactUsMessages|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactUsMessages[]    findAll()
 * @method ContactUsMessages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactUsMessagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactUsMessages::class);
    }
}
