<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Admin\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
 use VitaliiLuka\MentorshipBlog\Admin\Entity\Admin;

 class AdminFixtures extends Fixture
 {
     /**
      * UserFixtures constructor.
      * @param UserPasswordHasherInterface $passwordHasher
      * @ORM\Column(type="string")
      */
     private UserPasswordHasherInterface $passwordHasher;

     public function __construct(UserPasswordHasherInterface $passwordHasher)
     {
         $this->passwordHasher = $passwordHasher;
     }

     public function load(ObjectManager $manager): void
     {
         $user = new Admin(
             'admin@example.com',
             '12345678',
             ['ROLE_ADMIN'],
         );
         $user->setPassword($this->passwordHasher->hashPassword($user, '12345678'));

         $manager->persist($user);
         $manager->flush();
     }
 }
