<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Admin\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use VitaliiLuka\MentorshipBlog\Core\Entity\ContactUsMessages;

class ContactUsMessagesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ContactUsMessages::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
