<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Admin\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use VitaliiLuka\MentorshipBlog\Post\Entity\Tag;

class TagCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Tag::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
