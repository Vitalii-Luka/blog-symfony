<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Admin\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\Core\Entity\ContactUsMessages;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\Post\Entity\Category;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;
use VitaliiLuka\MentorshipBlog\Post\Entity\Tag;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Home', 'fa fa-home'),

            MenuItem::subMenu('User', 'fa fa-article')->setSubItems([
                MenuItem::linkToCrud('User', 'fa fa-file-pdf', User::class),
                MenuItem::linkToCrud('Contact Us', 'fa fa-file-pdf', ContactUsMessages::class),
            ]),

            MenuItem::subMenu('Content', 'fa fa-article')->setSubItems([
                MenuItem::linkToCrud('Posts', 'fa fa-file-pdf', Post::class),
                MenuItem::linkToCrud('Category', 'fa fa-tags', Category::class),
                MenuItem::linkToCrud('Tags', 'fa fa-tags', Tag::class),
            ]),
        ];
    }
}
