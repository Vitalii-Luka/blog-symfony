<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Admin\Controller\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AdminSecurityLoginController extends AbstractController
{
    /**
     * @Route("/admin/login", name="admin_app_login")
     */
    public function __invoke(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('admin/security/admin-login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }
}
