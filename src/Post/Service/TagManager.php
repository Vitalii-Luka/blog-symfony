<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Service;

use Doctrine\ORM\EntityManagerInterface;
use VitaliiLuka\MentorshipBlog\Post\Entity\Tag;

class TagManager
{
    private $repository;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->repository = $manager->getRepository(Tag::class);
    }

    public function getByName(string $name): Tag
    {
        $tag = $this->repository->findOneBy(['name' => $name]);

        if (!$tag) {
            $tag = (new Tag())->setName($name);
        }

        return $tag;
    }
}
