<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Entity;

use Doctrine\ORM\Mapping as ORM;
use VitaliiLuka\MentorshipBlog\Common\Entity\Traits\IdentifiableEntityTrait;

/**
 * @ORM\Entity(repositoryClass="VitaliiLuka\MentorshipBlog\Post\Repository\TagRepository")
 */
class Tag
{
    use IdentifiableEntityTrait;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $name;

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }
}
