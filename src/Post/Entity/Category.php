<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use VitaliiLuka\MentorshipBlog\Common\Entity\Traits\IdentifiableEntityTrait;

/**
 * @ORM\Entity(repositoryClass="VitaliiLuka\MentorshipBlog\Post\Repository\CategoryRepository")
 */
class Category
{
    use IdentifiableEntityTrait;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private string $name;

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Category
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }
}
