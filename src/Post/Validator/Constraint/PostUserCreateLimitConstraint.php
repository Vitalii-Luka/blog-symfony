<?php


namespace VitaliiLuka\MentorshipBlog\Post\Validator\Constraint;


use Symfony\Component\Validator\Constraint;
use VitaliiLuka\MentorshipBlog\Post\Validator\PostUserCreateLimitValidator;

/**
 * @Annotation
 */
class PostUserCreateLimitConstraint extends Constraint
{
    public string $limitPerMonthForFreeExceededMessage = 'Create posts without subscription limit exceeded.';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * @return string
     */
    public function validatedBy()
    {
        return PostUserCreateLimitValidator::class;
    }
}