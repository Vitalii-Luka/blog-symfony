<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Validator;

use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\Payment\Entity\Subscription;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;
use VitaliiLuka\MentorshipBlog\Post\Validator\Constraint\PostUserCreateLimitConstraint;

class PostUserCreateLimitValidator extends ConstraintValidator
{
    public const LIMIT_PER_MONTH_FOR_FREE = 5;

    protected EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Post $post
     * @param Constraint $constraint
     *
     * @throws Exception
     */
    public function validate($post, Constraint $constraint)
    {
        if (!$constraint instanceof PostUserCreateLimitConstraint)
        {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\PostUserCreateLimitConstraint');
        }

        $subscription = $this->manager->getRepository(Subscription::class)->findOneBy(['user' => $post->getAuthor()->getId()]);

        if ($subscription === null) {

            if ($this->isLimitPerMonthForFreeExceeded($post->getAuthor())) {

                $this
                    ->context
                    ->buildViolation($constraint->limitPerMonthForFreeExceededMessage)
                    ->setParameter('{{ limit }}', (string)self::LIMIT_PER_MONTH_FOR_FREE)
                    ->addViolation();

            }

        }

    }

    /**
     * @param User $user
     *
     * @return bool
     *
     * @throws Exception
     */
    public function isLimitPerMonthForFreeExceeded(User $user): bool
    {
        $from = DateTimeImmutable::createFromMutable(new DateTime(date('Y-m-01')));

        return $this
                ->manager
                ->getRepository(Post::class)
                ->getCountByUser($user, $from) >= self::LIMIT_PER_MONTH_FOR_FREE;
    }
}
