<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Dto;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\Post\Entity\Category;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;
use VitaliiLuka\MentorshipBlog\Post\Entity\Tag;
use VitaliiLuka\MentorshipBlog\Post\Validator\Constraint as PostAssert;

/**
 * @PostAssert\PostUserCreateLimitConstraint(groups={"Default"})
 */
class PostDTO
{
    /**
     * @Assert\Image(
     *     maxHeight=400,
     *     maxWidth=1600,
     *     mimeTypes={"image/jpeg", "image/png" },
     *     mimeTypesMessage="Please upload a jpeg or png image",
     *     maxSize="4M",
     * )
     */
    private ?UploadedFile $image = null;

    /**
     * @Assert\NotBlank()
     */
    private User $author;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=255)
     */
    private ?string $title;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=8, max=500)
     */
    private ?string $summary;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=8, max=10000)
     */
    private ?string $content;

    private ?Category $category;

    private array $tags;

    /**
     * @param null $title
     * @param null $summary
     * @param null $content
     * @param null $category
     */
    public function __construct(
        User $author,
        $title = null,
        $summary = null,
        $content = null,
        $category = null,
        array $tags = []
    ) {
        $this->author = $author;
        $this->title = $title;
        $this->summary = $summary;
        $this->content = $content;
        $this->category = $category;
        $this->tags = $tags;
    }

    public function getImage(): ?UploadedFile
    {
        return $this->image;
    }

    public function setImage(?UploadedFile $image): void
    {
        $this->image = $image;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory($category): void
    {
        $this->category = $category;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public static function fromEntity(Post $post): self
    {
        $tags = [];

        /** @var Tag $tag */
        foreach ($post->getTags() as $tag) {
            $tags[] = $tag->getName();
        }

        return new static(
            $post->getAuthor(),
            $post->getTitle(),
            $post->getSummary(),
            $post->getContent(),
            $post->getCategory(),
            $tags
        );
    }

    public function addTag($tag): void
    {
        $this->tags[] = $tag;
    }

    public function deleteTag($tag): void
    {
        // do smthng
    }
}
