<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class PostSearchData
{
    /**
     * @Assert\NotBlank()
     *
     * @var string|null
     */
    private $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return PostSearchData
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
