<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;

class PostController extends AbstractController
{
    /**
     * @Route("/post/{id}", name="post_post")
     */
    public function __invoke(Post $post)
    {
        return $this->render('post/post.html.twig', [
            'post' => $post,
        ]);
    }
}
