<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\Core\Service\FileUploader;
use VitaliiLuka\MentorshipBlog\Post\Dto\PostDTO;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;
use VitaliiLuka\MentorshipBlog\Post\Form\PostType;
use VitaliiLuka\MentorshipBlog\Post\Service\TagManager;

class PostEditController extends AbstractController
{
    private FileUploader $fileUploader;

    private TagManager $tagManager;

    public function __construct(TagManager $tagManager)
    {
        $this->tagManager = $tagManager;
    }

    /**
     * @Route("/post/edit/{id}", name="edit_post")
     * @return RedirectResponse|Response
     */
    public function __invoke(
        Post $post,
        Request $request,
        EntityManagerInterface $manager,
        FileUploader $fileUploader
    ): Response {
        $this->denyAccessUnlessGranted('edit_post', $post);

        $dto = PostDTO::fromEntity($post);

        $form = $this->createForm(PostType::class, $dto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $dto->getImage();
            foreach ($dto->getTags() as $tag) {
                $post->addTag($this->tagManager->getByName($tag));
            }

            // @TODO: implement upload images in the another directory

            if ($image) {
                $imageFileName = $fileUploader->upload($image);
                $post->setImage($imageFileName);
            }

            $post->setAuthor($dto->getAuthor());
            $post->setTitle($dto->getTitle());
            $post->setSummary($dto->getSummary());
            $post->setContent($dto->getContent());
            $post->setCategory($dto->getCategory());

            $tags = [];
            foreach ($dto->getTags() as $tag) {
                $tags[] = $this->tagManager->getByName($tag);
            }

            $post->setTags($tags);

            $manager->persist($post);
            $manager->flush();

            $this->addFlash('success', 'Post successfully updated!');

            return $this->redirectToRoute('home');
        }

        return $this->render('post/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
