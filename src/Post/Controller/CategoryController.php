<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\Post\Repository\CategoryRepository;

class CategoryController extends AbstractController
{
    private CategoryRepository $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/category", name="category")
     */
    public function __invoke(): Response
    {
        $categories = $this->repository->findAll();

        return $this->render('core/category/category.html.twig', [
            'categories' => $categories,
        ]);
    }
}
