<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\Post\Repository\CategoryRepository;

class CategoryPostsController extends AbstractController
{
    private CategoryRepository $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/category/{categoryName}", name="category_posts", methods={"GET"})
     * @ParamConverter("category", options={"mapping": {"categoryName": "name"}})
     */
    public function __invoke($categoryName): Response
    {
        $category = $this->repository->findOneBy(['name' => $categoryName]);

        return $this->render('core/category/categoryposts.html.twig', [
            'categoryPosts' => $category,
        ]);
    }
}
