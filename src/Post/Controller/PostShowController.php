<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;

class PostShowController extends AbstractController
{
    private $repository;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->repository = $manager->getRepository(Post::class);
    }

    /**
     * @Route("/show", name="show_post")
     */
    public function __invoke(EntityManagerInterface $manager, Request $request, PaginatorInterface $paginator): Response
    {
        $user = $this->getUser();

        $this->denyAccessUnlessGranted('view_post');

        $query = $this->repository->findByUserQuery($user);

        return $this->render('post/show.html.twig', [
            'posts' => $paginator
                ->paginate(
                    $query,
                    $request->query->getInt('page', 1),
                    5
                ),
        ]);
    }
}
