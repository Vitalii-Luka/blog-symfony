<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;

class PostDeleteController extends AbstractController
{
    /**
     * @Route("/post/delete/{id}", name="post_delete")
     * @ParamConverter("post", options={"id": "id"})
     */
    public function __invoke(Post $post, Request $request): Response
    {
        if (!$post) {
            throw new NotFoundHttpException();
        }
        $em = $this->getDoctrine()->getManager();

        $this->denyAccessUnlessGranted('delete_post', $post);

        $em->remove($post);
        $em->flush();

        $this->addFlash('success', 'Post successfully deleted!');

        return $this->redirectToRoute('home');

        return $this->render('post/delete.html.twig', [
        ]);
    }
}
