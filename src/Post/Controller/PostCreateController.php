<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\Core\Service\FileUploader;
use VitaliiLuka\MentorshipBlog\Post\Dto\PostDTO;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;
use VitaliiLuka\MentorshipBlog\Post\Form\PostType;
use VitaliiLuka\MentorshipBlog\Post\Service\TagManager;
use VitaliiLuka\MentorshipBlog\User\Service\UserManager;

class PostCreateController extends AbstractController
{
    private FileUploader $fileUploader;

    private $repository;

    private TagManager $tagManager;

    public function __construct(EntityManagerInterface $manager, TagManager $tagManager)
    {
        $this->repository = $manager->getRepository(User::class);
        $this->tagManager = $tagManager;
    }

    /**
     * @Route("/profile/create", name="create_post")
     *
     * @return RedirectResponse|Response
     */
    public function __invoke(
        Request $request,
        UserManager $userManager,
        EntityManagerInterface $manager,
        FileUploader $fileUploader
    ): Response {
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedException();
        }

        $author = $this->repository->find($user->getId());

        if (!$author) {
            throw new AccessDeniedException();
        }

        $this->denyAccessUnlessGranted('create_post');

        $dto = new PostDTO($author);
        $form = $this->createForm(PostType::class, $dto);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dto = $form->getData();
            $image = $dto->getImage();

            $post = new Post(
                $dto->getTitle(),
                $dto->getSummary(),
                $dto->getContent(),
                $dto->getAuthor(),
                $dto->getCategory(),
            );

            foreach ($dto->getTags() as $tag) {
                $post->addTag($this->tagManager->getByName($tag));
            }

            // @TODO: implement upload images in the another directory

            if ($image) {
                $imageFileName = $fileUploader->upload($image);
                $post->setImage($imageFileName);
            }

            $manager->persist($post);
            $manager->flush();

            $this->addFlash('success', 'Post successfully created!');

            return $this->redirectToRoute('home');
        }

        return $this->render('post/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
