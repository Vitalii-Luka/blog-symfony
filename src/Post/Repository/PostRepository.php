<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Repository;

use DateTime;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function findByUserQuery(User $user): Query
    {
        return $this
            ->createQueryBuilder('p')
            ->andWhere('p.author = :user')
            ->setParameters([
                'user' => $user->getId(),
            ])
            ->getQuery()
        ;
    }

    /**
     * @param User $user
     * @param DateTimeImmutable|null $from
     *
     * @return int
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getCountByUser(User $user, DateTimeImmutable $from = null) : int
    {
        $builder = $this
            ->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->andWhere('p.author = :user')
            ->setParameters([
                'user' => $user->getId(),
            ])
        ;

        if ($from !== null) {

            $builder
                ->andWhere('p.createdAt >= :from')
                ->setParameter('from', $from);

        }

        return (int) $builder->getQuery()->getSingleScalarResult();
    }

    public function findAllQuery(): Query
    {
        return $this
            ->createQueryBuilder('p')
            ->getQuery()
        ;
    }

    public function searchByPartialData(string $value = null): Query
    {
        $builder = $this->createQueryBuilder('p');

        if ($value) {
            $builder
                ->andWhere('p.title LIKE :value')
                ->orWhere('p.summary LIKE :value')
                ->setParameter('value', '%' . $value . '%')
            ;
        }

        return $builder->getQuery();
    }
}
