<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use VitaliiLuka\MentorshipBlog\Post\Entity\Tag;

/**
 * @method Tag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tag[]    findAll()
 * @method Tag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tag::class);
    }

//
//    /**
//     * @return Query
//     */
//    public function findAllQuery(): Query
//    {
//        return $this->createQueryBuilder('t')
//            ->orderBy('t.id', 'DESC')
//            ->getQuery()
//            ;
//    }
}
