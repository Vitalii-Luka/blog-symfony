<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use VitaliiLuka\MentorshipBlog\Post\Dto\PostDTO;
use VitaliiLuka\MentorshipBlog\Post\Entity\Category;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('category', EntityType::class, [
                'label' => 'category',
                'class' => Category::class,
                'choice_label' => 'name',
            ])
            ->add('title', TextType::class, [
                'label' => 'title',
                'attr' => [
                    'placeholder' => 'enter text',
                ],
            ])
            ->add('summary', TextType::class, [
                'label' => 'summary',
                'attr' => [
                    'placeholder' => 'enter description',
                ],
            ])
            ->add('content', TextareaType::class, [
                'label' => 'content',
                'attr' => [
                    'placeholder' => 'enter text',
                    'class' => 'tinymce',
                ],
            ])
            ->add('tags', TextType::class, [
                'attr' => [
                    'placeholder' => 'enter tags',
                ],
            ])
            ->add('image', FileType::class, [
                'label' => 'image',
                'required' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Submit',
                'attr' => [
                    'class' => 'btn btn-success',
                ],
            ])
        ;

        $builder
            ->get('tags')
            ->addModelTransformer(new CallbackTransformer(
                fn ($tagsAsArray) => implode(', ', $tagsAsArray),
                fn ($tagsAsString) => explode(', ', $tagsAsString)
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PostDTO::class,
        ]);
    }
}
