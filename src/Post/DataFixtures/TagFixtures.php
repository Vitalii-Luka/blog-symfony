<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use VitaliiLuka\MentorshipBlog\Post\Entity\Tag;

class TagFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        for ($i = 0; $i < 20; ++$i) {
            $tag = new Tag();
            $tag->setName($faker->word);
            $manager->persist($tag);
        }

        $manager->flush();
        $this->addReference('tag', $tag);
    }

    public function getOrder(): int
    {
        return 20;
    }
}
