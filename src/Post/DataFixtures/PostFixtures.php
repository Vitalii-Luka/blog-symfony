<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;
use VitaliiLuka\MentorshipBlog\User\DataFixtures\UserFixtures;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 1; $i < 20; $i++) {
            $post = new Post(
                $faker->title,
                $faker->realText(200, 2),
                $faker->realText(500, 2),
                $this->getReference(UserFixtures::POST_AUTHOR),
                $this->getReference('category')
            );

            $manager->persist($post);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [UserFixtures::class];
    }
}
