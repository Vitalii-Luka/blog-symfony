<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use VitaliiLuka\MentorshipBlog\Post\Entity\Category;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        foreach ($this->getCategory() as $index => $name) {
            $category = new Category();
            $category->setName($name);

            $manager->persist($category);
        }

        $manager->flush();
        $this->addReference('category', $category);
    }

    public function getCategory(): array
    {
        return [
            'Nature',
            'Urban',
            'Black and white',
            'Landscape',
            'Advertising',
            'Wedding',
            'HDR',
            'Fashion',
            'Sport',
            'Portrait',
        ];
    }
}
