<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;

class PostVoterView extends Voter
{
    public const VIEW = 'view_post';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if ($attribute !== self::VIEW) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        return $this->canView();
    }

    private function canView(): bool
    {
        return $this->security->isGranted('IS_AUTHENTICATED_FULLY');
    }
}
