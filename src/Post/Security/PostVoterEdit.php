<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;
use VitaliiLuka\MentorshipBlog\Post\Entity\Post;

class PostVoterEdit extends Voter
{
    public const EDIT = 'edit_post';

    protected function supports(string $attribute, $subject): bool
    {
        if ($attribute !== self::EDIT) {
            return false;
        }

        if (!$subject instanceof Post) {
            return false;
        }

        return true;
    }

    /**
     * @param Post $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        $post = $subject;

        return $this->canEdit($post, $user);
    }

    private function canEdit(Post $post, User $user): bool
    {
        return $user->getId() === $post->getAuthor()->getId();
    }
}
