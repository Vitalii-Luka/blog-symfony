<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Post\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;

class PostVoterCreate extends Voter
{
    public const CREATE = 'create_post';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if ($attribute !== self::CREATE) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        return $this->canCreate();
    }

    private function canCreate(): bool
    {
        return $this->security->isGranted('IS_AUTHENTICATED_FULLY');
    }
}
