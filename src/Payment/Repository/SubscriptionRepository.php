<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Payment\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use VitaliiLuka\MentorshipBlog\Payment\Entity\Subscription;

/**
 * @method Subscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Subscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Subscription[]    findAll()
 * @method Subscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Subscription::class);
    }

//
//    /**
//     * @return Query
//     */
//    public function findAllQuery(): Query
//    {
//        return $this->createQueryBuilder('t')
//            ->orderBy('t.id', 'DESC')
//            ->getQuery()
//            ;
//    }
}
