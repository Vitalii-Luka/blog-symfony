<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Payment\Controller;

use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\This;
use Psr\Log\LoggerInterface;
use Stripe\Exception\SignatureVerificationException;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Webhook;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use UnexpectedValueException;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;


class WebhookController
{
    private string $stripeSk;
    private string $endpointSecret;
    private EntityManagerInterface $manager;
    private LoggerInterface $logger;

    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger, string $stripeSk, string $endpointSecret)
    {
        $this->manager = $manager;
        $this->logger = $logger;
        $this->stripeSk = $stripeSk;
        $this->endpointSecret = $endpointSecret;
    }

    /**
     * @Route("/stripe/webhook", name="stripe_webhook", methods={"POST", "GET"})
     */
    public function __invoke(Request $request)
    {
        $this->logger->debug(sprintf('%s: %s', __CLASS__, ', WebHook received: ' . $request->__toString()));

        $response = new JsonResponse();

        Stripe::setApiKey($this->stripeSk);

        $endpointSecret = $this->endpointSecret;

        $event = null;
        try {

            $event = Webhook::constructEvent(
                $request->getContent(),
                $request->headers->get('STRIPE_SIGNATURE'),
                $endpointSecret
            );

        } catch(UnexpectedValueException $e) {
        } catch (SignatureVerificationException $e) {

            $this
                ->logger
                ->critical(sprintf('%s: %s', __CLASS__, $e->getMessage()));

            // Invalid payload
            $response->setStatusCode(400);

        }

        switch ($event->type) {
            case 'customer.subscription.created':
                $subscription = $event->data->object;
                $this->handleSubscriptionCreated($subscription);
                break;
            case 'customer.subscription.deleted':
                $subscription = $event->data->object;
                $this->handleSubscriptionDeleted($subscription);
                break;
            case 'customer.subscription.updated':
                $subscription = $event->data->object;
                $this->handleSubscriptionUpdate($subscription);
                break;
            default:
                break;
        }

        return $response;
    }

    private function handleSubscriptionCreated(Subscription $subscription)
    {
        $user = $this->manager->getRepository(User::class)->findOneBy(['email' => $subscription->customer->email]);

        $entity = new \VitaliiLuka\MentorshipBlog\Payment\Entity\Subscription();

        $entity
            ->setId($subscription->id)
            ->setUser($user)
            ->setExpirationDateTime($subscription->current_period_end);

        $this->manager->persist($entity);
        $this->manager->flush();
    }

    private function handleSubscriptionDeleted(Subscription $subscription)
    {
        $entity = $this->manager->getRepository(Subscription::class)->find($subscription->id);

        $this->manager->remove($entity);
        $this->manager->flush();
//        var_dump($subscription);
    }

    private function handleSubscriptionUpdate(Subscription $subscription)
    {
        $entity = $this->manager->getRepository(Subscription::class)->find($subscription->id);

        $this->manager->remove($entity);
        $this->manager->flush();
    }
}
