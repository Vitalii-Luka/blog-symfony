<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Payment\Controller;

use Stripe\Checkout\Session;
use Stripe\Exception\ApiErrorException;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use VitaliiLuka\MentorshipBlog\Payment\Service\CreateCustomer;

class CheckoutController extends AbstractController
{
    private string $stripeSk;
    private string  $priceKey;
    private CreateCustomer $stripeClient;
    public function __construct(string $stripeSk, string  $priceKey, CreateCustomer $stripeClient)
    {
        $this->stripeSk = $stripeSk;
        $this->priceKey = $priceKey;
        $this->stripeClient = $stripeClient;
    }

    /**
     * @Route("/checkout", name="payment_checkout")
     */
    public function __invoke(): Response
    {
        Stripe::setApiKey($this->stripeSk);

        $prices = $this->priceKey;



        try {
            $user = $this->getUser();
            $customer = $this->stripeClient->createCustomer($user);

        } catch (ApiErrorException $e) {

            // TODO:
            throw $e;

        }

        $checkout_session = Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price' => $prices,
                'quantity' => 1,
            ]],
            'customer' => $customer,
            'mode' => 'subscription',
            'success_url' => $this->generateUrl('payment_success_url', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => $this->generateUrl('payment_cancel_url', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        return $this->redirect($checkout_session->url, 303);
    }
}
