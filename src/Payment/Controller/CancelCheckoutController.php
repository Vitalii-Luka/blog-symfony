<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Payment\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CancelCheckoutController extends AbstractController
{
    /**
     * @Route("/cancel-url", name="payment_cancel_url")
     */
    public function cancelUrl(): Response
    {
        return $this->render('payment/cancel.html.twig', []);
    }
}
