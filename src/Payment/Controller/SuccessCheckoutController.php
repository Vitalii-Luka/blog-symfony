<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Payment\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SuccessCheckoutController extends AbstractController
{
    /**
     * @Route("/payment/success-url", name="payment_success_url")
     */
    public function cancelUrl(): Response
    {
        return $this->render('payment/success.html.twig', []);
    }
}
