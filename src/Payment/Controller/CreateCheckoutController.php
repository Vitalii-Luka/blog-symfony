<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Payment\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateCheckoutController extends AbstractController
{
    /**
     * @Route("/createcheckout", name="payment_createcheckout")
     */
    public function __invoke(): Response
    {
        return $this->render('payment/checkout.html.twig');
    }
}
