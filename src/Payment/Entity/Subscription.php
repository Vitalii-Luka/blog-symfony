<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Payment\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use VitaliiLuka\MentorshipBlog\Common\Entity\Traits\IdentifiableEntityTrait;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;

/**
 * @ORM\Entity()
 */
class Subscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected ?int $id = null;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string")
     */
    private string $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $expirationDateTime;

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     * @return Subscription
     */
    public function setUser(string $user): Subscription
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getExpirationDateTime(): DateTime
    {
        return $this->expirationDateTime;
    }

    /**
     * @param DateTime $expirationDateTime
     * @return Subscription
     */
    public function setExpirationDateTime(DateTime $expirationDateTime): Subscription
    {
        $this->expirationDateTime = $expirationDateTime;
        return $this;
    }
}