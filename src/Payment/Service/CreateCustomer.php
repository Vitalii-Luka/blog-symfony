<?php

declare(strict_types=1);

namespace VitaliiLuka\MentorshipBlog\Payment\Service;

use Doctrine\ORM\EntityManagerInterface;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use VitaliiLuka\MentorshipBlog\Core\Entity\User;

class CreateCustomer extends AbstractController
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @throws ApiErrorException
     */
    public function createCustomer(User $user): string
    {
        if ($user->getCustomerId()) {
            $customer = $user->getCustomerId();
        } else {
            $customer = Customer::create([
                'email' => $user->getEmail(),
                'name' => $user->getLastName() . ' ' . $user->getFirstName(),
            ])
                ->id;

            $user->setCustomerId($customer);
            $this->em->flush();
        }

        return $customer;
    }

}